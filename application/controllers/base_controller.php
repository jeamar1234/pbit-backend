<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_controller extends CI_Controller {

	protected $model_name = '';
	protected $default_view = 'blank';

	function __construct(){

		parent::__construct();
		$this->load->model($this->model_name,"model");
		
		
		if (isset($_SERVER['HTTP_ORIGIN'])) {
		    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		    header('Access-Control-Allow-Credentials: true');
		    header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

		    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		    exit(0);
		}
		
	}

	public function index($view = false)
	{	

		if($this->session->userdata('logged_in') == TRUE){
			if($view)
				$data['view'] = $view;
			else
				$data['view'] = $this->default_view;
				$this->load->view("index",$data);
				
		}else{
			if($view == 'create_account'){
				$this->load->view($view);
			}else{
				$this->load->view("login");
			}
			
		}
		
		
	}

	// BASIC FUNCTIONS
	public function get(){
		$data = $this->model->get();
		echo json_encode($data);
	}


	public function add(){
		$data = $this->model->add($_POST);
		echo $data;
	}
	
	public function update($col_id,$rowid){
		$data = $this->model->update($_POST,$col_id,$rowid);
		echo $data;
	}
	public function delete($col_id = FALSE, $id){
		if($col_id){
			$data = $this->model->delete($col_id,$id);
			echo $data;
		}else{
			$data = $this->model->delete($col_id = FALSE,$id);
			echo $data;
		}
	}


	public function do_upload()
    {
        $name_array = array();
        $count = count($_FILES['userfile']['size']);

        $config['upload_path'] = FCPATH . 'assets/img/uploads/';
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']    = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';


        $this->load->library('upload', $config);
	            
	        if ( ! $this->upload->do_upload()){
	        	echo $error = $this->upload->display_errors();
	        }else{
	        	$data = $this->upload->data();	
	        	// echo json_encode($data);
	        	$image_data = array(
	        			'image_name' => $data['file_name'],
	        			'image_question' => $_POST['image_question']
	        		);
	        	$this->model->add_images($image_data);
	        	echo json_encode("a");
	        }	        
    }

    public function get_images(){
    	$data['images'] = $this->model->get_images();
    	$data['view'] = $this->default_view;
    	// echo json_encode($data);
    	$this->load->view("index",$data);
    }

	public function login()
	{
		$data = $this->model->session($_POST);

		if($data != 0){
			echo json_encode($data);
			$this->session->set_userdata('logged_in',TRUE);
		}else{
			echo 0;
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();	
		redirect('/base_controller');
	}




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */