<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include APPPATH.'controllers/base_controller.php';

class home_con extends Base_Controller {
	protected $model_name = 'base_model';
	protected $default_view = 'home';
	
	function __construct(){
			parent::__construct();
	}
	
}