<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include APPPATH.'controllers/base_controller.php';

class Users_con extends Base_Controller {
	protected $model_name = 'users_model';
	protected $default_view = 'login';
	
	function __construct(){
			parent::__construct();
	}
	
}