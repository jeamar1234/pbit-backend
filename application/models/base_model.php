<?php

class Base_model extends CI_Model {

	protected $primary_key = '';
	protected $table_name = '';
	
	function __construct()
    {	
        parent::__construct();
    }

    // GET
    function get(){
    	return $this->db->from($this->table_name)->get()->result_object();
    }

    // ADD
    function add($data){
        if($this->db->insert($this->table_name,$data)){
            return $this->db->insert_id();
        }else{
            return 0;
        }
    }

    // EDIT
    function update($data,$col_id,$id){
        if($this->db->where($col_id,$id)->update($this->table_name,$data)){
            return 1;
        }else{
            return 0;
        }
    }

    // DELETE
    function delete($col_id,$id){
        if($col_id != ""){
            if($this->db->where($col_id,$id)->delete($this->table_name)){
                return 1;
            }else{
                return 0;
            }
        }else{
            if($this->db->where("id",$id)->delete($this->table_name)){
                return 1;
            }else{
                return 0;
            }
        }
    }

    //CUSTOM
    function session($cols){
        $data = $this->db->where($cols)->from($this->table_name)->get()->result_object();

        if( count($data) > 0 ){
            return $this->db->where($cols)->from($this->table_name)->get()->result_object();
        }else{
            return 0;
        }
    }

    function add_images($data){
        if($this->db->insert($this->table_name,$data)){
            return $this->db->insert_id();
        }else{
            return 0;
        }
    }
    function get_images(){
        return $this->db->from($this->table_name)->get()->result_object();
    }

}