<!DOCTYPE html>
<html lang="en">
<head>
	<title>PrecisionBit</title>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/fileinput.min.css" media="all">
	

</head>
<body>
	<header>
		<a id="logout-btn" class="btn btn-default" style="float:right;margin:3px 2px 0 0;" href='<?php echo site_url("base_controller/logout")?>'>Log out</a>
	</header>				

	<div class="content-wrapper">
		<section class="content">
			<div class="row">
				<div class="col-md-2 left-bar-wrapper">
					<div class="left-bar">
						<ul class="nav">
							<li><a id="view-btn" href='<?php echo site_url("images_con/get_images")?>'>View Survey</a></li>
							<li><a id="goUpload-btn" href='<?php echo site_url("home_con/index/home")?>'>Upload Survey</a></li>
							<li><a id="goUpload-btn">Survey Result</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-10">
					 <?php
			            $this->load->view($view);
			         ?>
			    </div>
			</div>
		</section>
	</div>

</body>
	<script type="text/javascript">
		var baseurl = "<?php print base_url(); ?>";
		var siteurl = "<?php print site_url(); ?>";
	</script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/canvas-to-blob.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/fileinput.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/app.js"></script>
		


</html>