<!DOCTYPE html>
<html lang="en">
<head>
	<title>PrecisionBit</title>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/css/font-awesome.min.css">
	

</head>
<body>
	<header>
	</header>				

	<div class="content-wrapper">
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3 login-wrapper">
						<div class="login">
							<!-- <form> -->
							  <div class="form-group">
							    <label for="exampleInputEmail1">Email</label>
							    <input type="email" class="form-control" id="email" >
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Password</label>
							    <input type="password" class="form-control" id="password">
							  </div>
							  <button id="login-btn" type="submit" class="btn btn-default">Submit</button>
							  <a href="<?php echo site_url("users_con/index/create_account")?>">Create account</a>
							<!-- </form> -->
						</div>
						<br>
						<div class="message">
							<div class="alert alert-warning" role="alert" style="display:none;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

</body>
	<script type="text/javascript">
		var baseurl = "<?php print base_url(); ?>";
		var siteurl = "<?php print site_url(); ?>";
	</script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/jquery.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/canvas-to-blob.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/fileinput.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/app.js"></script>
</html>