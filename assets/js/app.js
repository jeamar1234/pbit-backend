$("#create-submit-btn").click(function(){

	var name = $("#user-name").val();
	var email = $("#user-email").val();
	var pass = $("#user-password").val();

			$.ajax({
				url: siteurl + '/users_con/add',
				type: 'POST',
				dataType:'JSON',
				data:{'user_name': name,'user_email': email,'user_password': pass},
				success: function(data) {
					console.log(data);
					// location.reload();
					$(".message .alert").show();
					if(data > 0 ){
						$(".message .alert").html("Success !");
					}else{
						$(".message .alert").html("Error !");
					}
				},
				error: function(e) {
					console.log(e.message);
				}
			});
		});

$("#login-btn").click(function(){

	var email = $("#email").val();
	var pass = $("#pass").val();

			$.ajax({
				url: siteurl + '/users_con/login',
				type: 'POST',
				dataType:'JSON',
				data:{'user_email': email,'user_password': pass},
				success: function(data) {
					console.log(data);
					
					if(data != 0){
						location.reload();
					}else{
						console.log("error");
						$(".message .alert").show();
						$(".message .alert").html("Invalid Username or Password !");
					}
					
				},
				error: function(e) {
					console.log(e.message);
				}
			});
		});

$("#logout-btn").click(function(){
			$.ajax({
				url: siteurl + '/base_controller/logout',
				type: 'POST',
				success: function(data) {
					console.log(data);
					// location.reload();
				},
				error: function(e) {
					console.log(e.message);
				}
			});
		});

//IMAGE UPLOAD
		var img_text = 'a';

		var footerTemplate = '<div class="file-thumbnail-footer">\n' +
		'   <div style="margin:5px 0">\n' +
		'       <input class="kv-input kv-init form-control input-sm {TAG_CSS_INIT}" value="" placeholder="Enter Question...">\n' +
		'   </div>\n' +
		'   {actions}\n' +
		'</div>';
		$("#input-id").fileinput({	
			uploadUrl: siteurl + "/images_con/do_upload", // server upload action
    		uploadAsync: true,
    		layoutTemplates: {footer: footerTemplate},
    		uploadExtraData: function(previewId,index) {  // callback example

		        var out = {'image_question' : $('.kv-input:eq('+index+')').val() };
		        console.log(out);
		        console.log(index);
		        return out;

		    }
		}).on('filepreupload', function(event, data, previewId, index) {
			$('#kv-success-1').html('<h4>Upload Status</h4><ul></ul>').hide();
		}).on('fileuploaded', function(event, data, id, index) {
		    var fname = data.files[index].name,
		        out = '<li>' + 'Uploaded file # ' + (index + 1) + ' - '  +  
		            fname + ' successfully.' + '</li>';
		    $('#kv-success-1 ul').append(out);
		    $('#kv-success-1').fadeIn('slow');
		}).on("filepredelete", function(jqXHR) {
		    var abort = true;
		    if (confirm("Are you sure you want to delete this image?")) {
		        abort = false;
		    }
		    return abort; // you can also send any data/object that you can receive on `filecustomerror` event
		});


// DELETE IMAGE

$(".del-img-btn").click(function(){
	var select = $(this).closest('.images-wrapper');
	var id = $(this).closest('.options').find('input').val();
	console.log(id);
		$.ajax({
			url: siteurl + '/images_con/delete/image_id/' + id,
			type: 'POST',
			success: function(data) {
				console.log(data);
				// location.reload();
				select.fadeOut();
			},
			error: function(e) {
				console.log(e.message);
			}
		});

});

$(".edit-img-btn").click(function(){
	var select = $(this).closest('.images-wrapper').find('.image_question');
	var val = $(this).closest('.images-wrapper').find('.image_question').text()	;
	var id = $(this).closest('.options').find('input').val();
	$(this).closest('.options').find('.edit-img-btn').hide();
	$(this).closest('.options').find('.done-edit-img-btn').css('display','inline');
	select.replaceWith('<input type="text" class="form-control image_question" value="' + val + '">');
	console.log(id + ' ' + val);

});

$(".done-edit-img-btn").click(function(){
	var thiss = $(this);
	var select = $(this).closest('.images-wrapper').find('.image_question');
	var val = $(this).closest('.images-wrapper').find('.image_question').val()	;
	var id = $(this).closest('.options').find('input').val();
	
	console.log(id + ' ' + val);

	$.ajax({
			url: siteurl + '/images_con/update/image_id/' + id,
			type: 'POST',
			dataType: 'JSON',
			data: { 'image_question' : val },
			success: function(data) {
				console.log(data);
				// location.reload();
				thiss.closest('.options').find('.edit-img-btn').show();
				thiss.closest('.options').find('.done-edit-img-btn').css('display','none');
				select.replaceWith('<div class="image_question">' + val + '</div>');
			},
			error: function(e) {
				console.log(e.message);
			}
		});

});
